﻿using School.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Services
{
    public static class ScheduleService
    {
        public static ICollection<Course> GetSchedule(Academic person)
        {
            return person.Courses.OrderBy(c => c.Date.Day).OrderBy(c => c.Date.Start).ToList();
        }

        public static void someothermethod()
        {
            Console.WriteLine();
        }

        public static void anothermethod()
        {
            Console.WriteLine("another method, but new branch");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entities
{
    public class Academic
    {
        public Academic()
        {
            Courses = new List<Course>();
            this.Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public List<Course> Courses { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entities
{
    public class Grade
    {
        public int Score { get; set; }
        public Course Course { get; set; }
        public string Name { get { return ToString(); } }

        public override string ToString()
        {
            return string.Format(Course.Name);
        }
    }

    public enum Score
    {
        Excellent = 6,
        NotExcellentAtAll = 2
    }
}

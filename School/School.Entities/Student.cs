﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entities
{
    public class Student : Academic
    {
        public Student()
        {
            this.Grades = new List<Grade>();
        }

        public List<Grade> Grades { get; set; }
    }
}

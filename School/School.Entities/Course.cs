﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entities
{
    public class Course
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public CourseDate Date { get; set; }
    }

    public class CourseDate
    {
        public DayOfWeek Day { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }
}

﻿namespace School
{
    partial class highSchoolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.middleNameLabel = new System.Windows.Forms.Label();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.ageLabel = new System.Windows.Forms.Label();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.middleNameTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.ageTextBox = new System.Windows.Forms.TextBox();
            this.teacherRadioButton = new System.Windows.Forms.RadioButton();
            this.studentRadioButton = new System.Windows.Forms.RadioButton();
            this.addPersonButton = new System.Windows.Forms.Button();
            this.firstNameErrorMessageLabel = new System.Windows.Forms.Label();
            this.middleNameErrorMessageLabel = new System.Windows.Forms.Label();
            this.lastNameErrorMessageLabel = new System.Windows.Forms.Label();
            this.ageErrorMessageLabel = new System.Windows.Forms.Label();
            this.radioButtonErrorMessageLabel = new System.Windows.Forms.Label();
            this.successfullyAddedPersonLabel = new System.Windows.Forms.Label();
            this.listViewCourse = new System.Windows.Forms.ListView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageAdd = new System.Windows.Forms.TabPage();
            this.groupBoxType = new System.Windows.Forms.GroupBox();
            this.labelCourses = new System.Windows.Forms.Label();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.labelSchedule = new System.Windows.Forms.Label();
            this.rtbSchedule = new System.Windows.Forms.RichTextBox();
            this.viewTeachersButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.firstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.middleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewStudentsButton = new System.Windows.Forms.Button();
            this.tabPageGrades = new System.Windows.Forms.TabPage();
            this.buttonSave = new System.Windows.Forms.Button();
            this.dataGridViewGrades = new System.Windows.Forms.DataGridView();
            this.Course = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelChooseStudent = new System.Windows.Forms.Label();
            this.comboBoxStudents = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPageAdd.SuspendLayout();
            this.groupBoxType.SuspendLayout();
            this.tabPageInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageGrades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).BeginInit();
            this.SuspendLayout();
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(105, 14);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(57, 13);
            this.firstNameLabel.TabIndex = 0;
            this.firstNameLabel.Text = "First Name";
            // 
            // middleNameLabel
            // 
            this.middleNameLabel.AutoSize = true;
            this.middleNameLabel.Location = new System.Drawing.Point(93, 46);
            this.middleNameLabel.Name = "middleNameLabel";
            this.middleNameLabel.Size = new System.Drawing.Size(69, 13);
            this.middleNameLabel.TabIndex = 1;
            this.middleNameLabel.Text = "Middle Name";
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(104, 80);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(58, 13);
            this.lastNameLabel.TabIndex = 2;
            this.lastNameLabel.Text = "Last Name";
            // 
            // ageLabel
            // 
            this.ageLabel.AutoSize = true;
            this.ageLabel.Location = new System.Drawing.Point(136, 114);
            this.ageLabel.Name = "ageLabel";
            this.ageLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ageLabel.Size = new System.Drawing.Size(26, 13);
            this.ageLabel.TabIndex = 3;
            this.ageLabel.Text = "Age";
            this.ageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(177, 11);
            this.firstNameTextBox.MaxLength = 20;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(234, 20);
            this.firstNameTextBox.TabIndex = 4;
            this.firstNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NamesTextBoxs_KeyPress);
            this.firstNameTextBox.Leave += new System.EventHandler(this.NamesTextBox_Leave);
            this.firstNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.firstNameTextBox_Validating);
            // 
            // middleNameTextBox
            // 
            this.middleNameTextBox.Location = new System.Drawing.Point(177, 43);
            this.middleNameTextBox.MaxLength = 20;
            this.middleNameTextBox.Name = "middleNameTextBox";
            this.middleNameTextBox.Size = new System.Drawing.Size(234, 20);
            this.middleNameTextBox.TabIndex = 5;
            this.middleNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NamesTextBoxs_KeyPress);
            this.middleNameTextBox.Leave += new System.EventHandler(this.NamesTextBox_Leave);
            this.middleNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.middleNameTextBox_Validating);
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(177, 77);
            this.lastNameTextBox.MaxLength = 20;
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(234, 20);
            this.lastNameTextBox.TabIndex = 6;
            this.lastNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NamesTextBoxs_KeyPress);
            this.lastNameTextBox.Leave += new System.EventHandler(this.NamesTextBox_Leave);
            this.lastNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.lastNameTextBox_Validating);
            // 
            // ageTextBox
            // 
            this.ageTextBox.Location = new System.Drawing.Point(177, 111);
            this.ageTextBox.MaxLength = 2;
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.Size = new System.Drawing.Size(234, 20);
            this.ageTextBox.TabIndex = 7;
            this.ageTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AgeTextBox_KeyPress);
            this.ageTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.ageTextBox_Validating);
            // 
            // teacherRadioButton
            // 
            this.teacherRadioButton.AutoSize = true;
            this.teacherRadioButton.Location = new System.Drawing.Point(6, 22);
            this.teacherRadioButton.Name = "teacherRadioButton";
            this.teacherRadioButton.Size = new System.Drawing.Size(65, 17);
            this.teacherRadioButton.TabIndex = 8;
            this.teacherRadioButton.TabStop = true;
            this.teacherRadioButton.Text = "Teacher";
            this.teacherRadioButton.UseVisualStyleBackColor = true;
            // 
            // studentRadioButton
            // 
            this.studentRadioButton.AutoSize = true;
            this.studentRadioButton.Location = new System.Drawing.Point(77, 22);
            this.studentRadioButton.Name = "studentRadioButton";
            this.studentRadioButton.Size = new System.Drawing.Size(62, 17);
            this.studentRadioButton.TabIndex = 9;
            this.studentRadioButton.TabStop = true;
            this.studentRadioButton.Text = "Student";
            this.studentRadioButton.UseVisualStyleBackColor = true;
            // 
            // addPersonButton
            // 
            this.addPersonButton.Location = new System.Drawing.Point(177, 244);
            this.addPersonButton.Name = "addPersonButton";
            this.addPersonButton.Size = new System.Drawing.Size(234, 23);
            this.addPersonButton.TabIndex = 10;
            this.addPersonButton.Text = "Add Person";
            this.addPersonButton.UseVisualStyleBackColor = true;
            this.addPersonButton.Click += new System.EventHandler(this.AddPeopleButton_Click);
            // 
            // firstNameErrorMessageLabel
            // 
            this.firstNameErrorMessageLabel.AutoSize = true;
            this.firstNameErrorMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.firstNameErrorMessageLabel.Location = new System.Drawing.Point(162, 32);
            this.firstNameErrorMessageLabel.Name = "firstNameErrorMessageLabel";
            this.firstNameErrorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.firstNameErrorMessageLabel.TabIndex = 14;
            // 
            // middleNameErrorMessageLabel
            // 
            this.middleNameErrorMessageLabel.AutoSize = true;
            this.middleNameErrorMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.middleNameErrorMessageLabel.Location = new System.Drawing.Point(162, 74);
            this.middleNameErrorMessageLabel.Name = "middleNameErrorMessageLabel";
            this.middleNameErrorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.middleNameErrorMessageLabel.TabIndex = 15;
            // 
            // lastNameErrorMessageLabel
            // 
            this.lastNameErrorMessageLabel.AutoSize = true;
            this.lastNameErrorMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.lastNameErrorMessageLabel.Location = new System.Drawing.Point(165, 117);
            this.lastNameErrorMessageLabel.Name = "lastNameErrorMessageLabel";
            this.lastNameErrorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.lastNameErrorMessageLabel.TabIndex = 16;
            // 
            // ageErrorMessageLabel
            // 
            this.ageErrorMessageLabel.AutoSize = true;
            this.ageErrorMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.ageErrorMessageLabel.Location = new System.Drawing.Point(165, 160);
            this.ageErrorMessageLabel.Name = "ageErrorMessageLabel";
            this.ageErrorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.ageErrorMessageLabel.TabIndex = 17;
            // 
            // radioButtonErrorMessageLabel
            // 
            this.radioButtonErrorMessageLabel.AutoSize = true;
            this.radioButtonErrorMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.radioButtonErrorMessageLabel.Location = new System.Drawing.Point(165, 209);
            this.radioButtonErrorMessageLabel.Name = "radioButtonErrorMessageLabel";
            this.radioButtonErrorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.radioButtonErrorMessageLabel.TabIndex = 18;
            // 
            // successfullyAddedPersonLabel
            // 
            this.successfullyAddedPersonLabel.AutoSize = true;
            this.successfullyAddedPersonLabel.ForeColor = System.Drawing.Color.Red;
            this.successfullyAddedPersonLabel.Location = new System.Drawing.Point(165, 226);
            this.successfullyAddedPersonLabel.Name = "successfullyAddedPersonLabel";
            this.successfullyAddedPersonLabel.Size = new System.Drawing.Size(0, 13);
            this.successfullyAddedPersonLabel.TabIndex = 19;
            // 
            // listViewCourse
            // 
            this.listViewCourse.Location = new System.Drawing.Point(177, 196);
            this.listViewCourse.Name = "listViewCourse";
            this.listViewCourse.Size = new System.Drawing.Size(234, 40);
            this.listViewCourse.TabIndex = 22;
            this.listViewCourse.UseCompatibleStateImageBehavior = false;
            this.listViewCourse.Validating += new System.ComponentModel.CancelEventHandler(this.listViewCourse_Validating);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageAdd);
            this.tabControl1.Controls.Add(this.tabPageInfo);
            this.tabControl1.Controls.Add(this.tabPageGrades);
            this.tabControl1.Location = new System.Drawing.Point(4, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(528, 298);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPageAdd
            // 
            this.tabPageAdd.Controls.Add(this.groupBoxType);
            this.tabPageAdd.Controls.Add(this.labelCourses);
            this.tabPageAdd.Controls.Add(this.successfullyAddedPersonLabel);
            this.tabPageAdd.Controls.Add(this.firstNameLabel);
            this.tabPageAdd.Controls.Add(this.radioButtonErrorMessageLabel);
            this.tabPageAdd.Controls.Add(this.middleNameLabel);
            this.tabPageAdd.Controls.Add(this.ageErrorMessageLabel);
            this.tabPageAdd.Controls.Add(this.listViewCourse);
            this.tabPageAdd.Controls.Add(this.lastNameErrorMessageLabel);
            this.tabPageAdd.Controls.Add(this.lastNameLabel);
            this.tabPageAdd.Controls.Add(this.middleNameErrorMessageLabel);
            this.tabPageAdd.Controls.Add(this.ageLabel);
            this.tabPageAdd.Controls.Add(this.firstNameErrorMessageLabel);
            this.tabPageAdd.Controls.Add(this.firstNameTextBox);
            this.tabPageAdd.Controls.Add(this.middleNameTextBox);
            this.tabPageAdd.Controls.Add(this.lastNameTextBox);
            this.tabPageAdd.Controls.Add(this.ageTextBox);
            this.tabPageAdd.Controls.Add(this.addPersonButton);
            this.tabPageAdd.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdd.Name = "tabPageAdd";
            this.tabPageAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdd.Size = new System.Drawing.Size(520, 272);
            this.tabPageAdd.TabIndex = 0;
            this.tabPageAdd.Text = "Add";
            this.tabPageAdd.UseVisualStyleBackColor = true;
            // 
            // groupBoxType
            // 
            this.groupBoxType.Controls.Add(this.teacherRadioButton);
            this.groupBoxType.Controls.Add(this.studentRadioButton);
            this.groupBoxType.Location = new System.Drawing.Point(177, 137);
            this.groupBoxType.Name = "groupBoxType";
            this.groupBoxType.Size = new System.Drawing.Size(234, 53);
            this.groupBoxType.TabIndex = 24;
            this.groupBoxType.TabStop = false;
            this.groupBoxType.Text = "Type";
            this.groupBoxType.Validating += new System.ComponentModel.CancelEventHandler(this.groupBoxType_Validating);
            // 
            // labelCourses
            // 
            this.labelCourses.AutoSize = true;
            this.labelCourses.Location = new System.Drawing.Point(88, 208);
            this.labelCourses.Name = "labelCourses";
            this.labelCourses.Size = new System.Drawing.Size(78, 13);
            this.labelCourses.TabIndex = 23;
            this.labelCourses.Text = "Select Courses";
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.labelSchedule);
            this.tabPageInfo.Controls.Add(this.rtbSchedule);
            this.tabPageInfo.Controls.Add(this.viewTeachersButton);
            this.tabPageInfo.Controls.Add(this.dataGridView1);
            this.tabPageInfo.Controls.Add(this.viewStudentsButton);
            this.tabPageInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInfo.Size = new System.Drawing.Size(520, 272);
            this.tabPageInfo.TabIndex = 2;
            this.tabPageInfo.Text = "Info";
            this.tabPageInfo.UseVisualStyleBackColor = true;
            // 
            // labelSchedule
            // 
            this.labelSchedule.AutoSize = true;
            this.labelSchedule.Location = new System.Drawing.Point(404, 10);
            this.labelSchedule.Name = "labelSchedule";
            this.labelSchedule.Size = new System.Drawing.Size(52, 13);
            this.labelSchedule.TabIndex = 30;
            this.labelSchedule.Text = "Schedule";
            // 
            // rtbSchedule
            // 
            this.rtbSchedule.Enabled = false;
            this.rtbSchedule.Location = new System.Drawing.Point(403, 29);
            this.rtbSchedule.Name = "rtbSchedule";
            this.rtbSchedule.Size = new System.Drawing.Size(109, 201);
            this.rtbSchedule.TabIndex = 29;
            this.rtbSchedule.Text = "";
            // 
            // viewTeachersButton
            // 
            this.viewTeachersButton.Location = new System.Drawing.Point(187, 237);
            this.viewTeachersButton.Name = "viewTeachersButton";
            this.viewTeachersButton.Size = new System.Drawing.Size(175, 23);
            this.viewTeachersButton.TabIndex = 28;
            this.viewTeachersButton.Text = "View Teachers";
            this.viewTeachersButton.UseVisualStyleBackColor = true;
            this.viewTeachersButton.Click += new System.EventHandler(this.viewTeachers_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstName,
            this.middleName,
            this.lastName,
            this.age});
            this.dataGridView1.Location = new System.Drawing.Point(6, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(391, 224);
            this.dataGridView1.TabIndex = 27;
            // 
            // firstName
            // 
            this.firstName.HeaderText = "First Name";
            this.firstName.Name = "firstName";
            // 
            // middleName
            // 
            this.middleName.HeaderText = "Middle Name";
            this.middleName.Name = "middleName";
            // 
            // lastName
            // 
            this.lastName.HeaderText = "Last Name";
            this.lastName.Name = "lastName";
            // 
            // age
            // 
            this.age.HeaderText = "Age";
            this.age.Name = "age";
            this.age.Width = 50;
            // 
            // viewStudentsButton
            // 
            this.viewStudentsButton.Location = new System.Drawing.Point(6, 237);
            this.viewStudentsButton.Name = "viewStudentsButton";
            this.viewStudentsButton.Size = new System.Drawing.Size(175, 23);
            this.viewStudentsButton.TabIndex = 26;
            this.viewStudentsButton.Text = "View Students";
            this.viewStudentsButton.UseVisualStyleBackColor = true;
            this.viewStudentsButton.Click += new System.EventHandler(this.ViewStudents_Click);
            // 
            // tabPageGrades
            // 
            this.tabPageGrades.Controls.Add(this.buttonSave);
            this.tabPageGrades.Controls.Add(this.dataGridViewGrades);
            this.tabPageGrades.Controls.Add(this.labelChooseStudent);
            this.tabPageGrades.Controls.Add(this.comboBoxStudents);
            this.tabPageGrades.Location = new System.Drawing.Point(4, 22);
            this.tabPageGrades.Name = "tabPageGrades";
            this.tabPageGrades.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGrades.Size = new System.Drawing.Size(520, 272);
            this.tabPageGrades.TabIndex = 1;
            this.tabPageGrades.Text = "Grades";
            this.tabPageGrades.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(185, 11);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 3;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // dataGridViewGrades
            // 
            this.dataGridViewGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGrades.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Course,
            this.Score});
            this.dataGridViewGrades.Location = new System.Drawing.Point(12, 38);
            this.dataGridViewGrades.Name = "dataGridViewGrades";
            this.dataGridViewGrades.Size = new System.Drawing.Size(506, 228);
            this.dataGridViewGrades.TabIndex = 2;
            // 
            // Course
            // 
            this.Course.HeaderText = "Course";
            this.Course.Name = "Course";
            this.Course.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            // 
            // labelChooseStudent
            // 
            this.labelChooseStudent.AutoSize = true;
            this.labelChooseStudent.Location = new System.Drawing.Point(9, 14);
            this.labelChooseStudent.Name = "labelChooseStudent";
            this.labelChooseStudent.Size = new System.Drawing.Size(44, 13);
            this.labelChooseStudent.TabIndex = 1;
            this.labelChooseStudent.Text = "Student";
            // 
            // comboBoxStudents
            // 
            this.comboBoxStudents.FormattingEnabled = true;
            this.comboBoxStudents.Location = new System.Drawing.Point(58, 11);
            this.comboBoxStudents.Name = "comboBoxStudents";
            this.comboBoxStudents.Size = new System.Drawing.Size(121, 21);
            this.comboBoxStudents.TabIndex = 0;
            this.comboBoxStudents.SelectedIndexChanged += new System.EventHandler(this.comboBoxStudents_SelectedIndexChanged);
            this.comboBoxStudents.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStudents_SelectionChangeCommitted);
            // 
            // highSchoolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(535, 304);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "highSchoolForm";
            this.Text = "High School";
            this.tabControl1.ResumeLayout(false);
            this.tabPageAdd.ResumeLayout(false);
            this.tabPageAdd.PerformLayout();
            this.groupBoxType.ResumeLayout(false);
            this.groupBoxType.PerformLayout();
            this.tabPageInfo.ResumeLayout(false);
            this.tabPageInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageGrades.ResumeLayout(false);
            this.tabPageGrades.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGrades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label middleNameLabel;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label ageLabel;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox middleNameTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox ageTextBox;
        private System.Windows.Forms.RadioButton teacherRadioButton;
        private System.Windows.Forms.RadioButton studentRadioButton;
        private System.Windows.Forms.Button addPersonButton;
        private System.Windows.Forms.Label firstNameErrorMessageLabel;
        private System.Windows.Forms.Label middleNameErrorMessageLabel;
        private System.Windows.Forms.Label lastNameErrorMessageLabel;
        private System.Windows.Forms.Label ageErrorMessageLabel;
        private System.Windows.Forms.Label radioButtonErrorMessageLabel;
        private System.Windows.Forms.Label successfullyAddedPersonLabel;
        private System.Windows.Forms.ListView listViewCourse;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageAdd;
        private System.Windows.Forms.TabPage tabPageGrades;
        private System.Windows.Forms.DataGridView dataGridViewGrades;
        private System.Windows.Forms.Label labelChooseStudent;
        private System.Windows.Forms.ComboBox comboBoxStudents;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Course;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.Label labelSchedule;
        private System.Windows.Forms.RichTextBox rtbSchedule;
        private System.Windows.Forms.Button viewTeachersButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn middleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn age;
        private System.Windows.Forms.Button viewStudentsButton;
        private System.Windows.Forms.Label labelCourses;
        private System.Windows.Forms.GroupBox groupBoxType;
    }
}


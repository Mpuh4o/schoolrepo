﻿using School.Data;
using School.Entities;
using School.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School
{
    public partial class highSchoolForm : Form
    {
        InMemoryStorage storage;

        public Student Student { get; set; }

        private ErrorProvider firstNameErrorProvider;
        private ErrorProvider middleNameErrorProvider;
        private ErrorProvider lastNameErrorProvider;
        private ErrorProvider ageErrorProvider;
        private ErrorProvider typeErrorProvider;
        private ErrorProvider coursesErrorProvider;

        public highSchoolForm()
        {
            InitializeComponent();

            storage = new InMemoryStorage();

            InitializeErrorProviders();
            PopulateCourseListView();
            AddScheduleButtonToGrid();

            PopulateStudentsCombo();

            studentRadioButton.Checked = true;
            buttonSave.Enabled = false;
        }

        private void AddPeopleButton_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Visible))
            {
                int age = Int16.Parse(ageTextBox.Text);

                if(teacherRadioButton.Checked == true)
                {
                    var teacher = new Teacher { FirstName = firstNameTextBox.Text, MiddleName = middleNameTextBox.Text, LastName = lastNameTextBox.Text, Age = Int16.Parse(ageTextBox.Text) };
                    teacher.Courses = GetSelectedCourses();
                    storage.Teachers.Create(teacher);
                    successfullyAddedPersonLabel.Text = "Teacher Added successfully ";
                }
                else
                {
                    var student = new Student { FirstName = firstNameTextBox.Text, MiddleName = middleNameTextBox.Text, LastName = lastNameTextBox.Text, Age = Int16.Parse(ageTextBox.Text) };
                    student.Courses = GetSelectedCourses();
                    storage.Students.Create(student);
                    successfullyAddedPersonLabel.Text = "Student Added successfully ";
                }

                ClearTextBoxAndRadioButtonFields();
            }
        }

        private void AddScheduleButtonToGrid()
        {
            DataGridViewButtonColumn schedule = new DataGridViewButtonColumn();
            schedule.Name = "Schedule";
            schedule.Text = "Schedule";
            dataGridView1.Columns.Insert(4, schedule);
        }

        private void DataGridSetUp(BindingSource source)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.DataSource = source;
            dataGridView1.Columns[0].DataPropertyName = "FirstName";
            dataGridView1.Columns[1].DataPropertyName = "MiddleName";
            dataGridView1.Columns[2].DataPropertyName = "LastName";
            dataGridView1.Columns[3].DataPropertyName = "Age";

            ClearTextBoxAndRadioButtonFields();
        }

        private void ViewStudents_Click(object sender, EventArgs e)
        {
            BindingList<Student> newBindingList = new BindingList<Student>(storage.Students.GetAll());
            var source = new BindingSource(newBindingList, null);

            DataGridSetUp(source);

            dataGridView1.CellClick -= dataGridView1Teacher_CellClick;
            dataGridView1.CellClick -= dataGridView1Student_CellClick;

            dataGridView1.CellClick += dataGridView1Student_CellClick;
        }

        private void viewTeachers_Click(object sender, EventArgs e)
        {
            BindingList<Teacher> newBindingList = new BindingList<Teacher>(storage.Teachers.GetAll());
            var source = new BindingSource(newBindingList, null);

            DataGridSetUp(source);
            dataGridView1.CellClick -= dataGridView1Student_CellClick;
            dataGridView1.CellClick -= dataGridView1Teacher_CellClick;

            dataGridView1.CellClick += dataGridView1Teacher_CellClick;
        }

        private void dataGridView1Student_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Schedule"].Index)
            {
                var rows = dataGridView1.SelectedRows;
                if (rows.Count == 0)
                {
                    throw new Exception("there's no row selected");
                }

                var firstName = rows[0].Cells[0].Value.ToString();
                var lastName = rows[0].Cells[2].Value.ToString();

                var student = storage.Students.GetAll().Where(s => s.FirstName == firstName).Where(s => s.LastName == lastName).FirstOrDefault();

                PrepareSchedule(student);
            }
        }

        private void dataGridView1Teacher_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Schedule"].Index)
            {
                var rows = dataGridView1.SelectedRows;
                if (rows.Count == 0)
                {
                    throw new Exception("there's no row selected");
                }

                var firstName = rows[0].Cells[0].Value.ToString();
                var lastName = rows[0].Cells[2].Value.ToString();

                var teacher = storage.Teachers.GetAll().Where(s => s.FirstName == firstName).Where(s => s.LastName == lastName).FirstOrDefault();

                PrepareSchedule(teacher);
            }
        }

        private void PrepareSchedule(Academic person)
        {
            var schecdule = ScheduleService.GetSchedule(person);

            rtbSchedule.Clear();

            foreach (var item in schecdule)
            {
                rtbSchedule.AppendText(item.Date.Day + " " + item.Date.Start + " " + item.Date.End + " - " + item.Name + "\n");
            }
        }

        private void AgeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[^0-9]+") && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void NamesTextBoxs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[^a-zA-Z-`']+") && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void NamesTextBox_Leave(object sender, EventArgs e)
        {
            TextBox newTextBox = (TextBox)sender;
            string assignValueOfTextBox = newTextBox.Text;
            newTextBox.Text = assignValueOfTextBox;
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            newTextBox.Text = textInfo.ToTitleCase(newTextBox.Text);
        }

        private void ClearTextBoxAndRadioButtonFields()
        {
            firstNameTextBox.Clear();
            middleNameTextBox.Clear();
            lastNameTextBox.Clear();
            ageTextBox.Clear();
            teacherRadioButton.Checked = false;
            studentRadioButton.Checked = false;
        }

        private void PopulateCourseListView()
        {
            foreach (var course in storage.Courses.GetAll())
            {
                listViewCourse.Items.Add(course.Name, course.Id);
            }
        }

        private List<Course> GetSelectedCourses()
        {
            var courseCollection = new List<Course>();

            foreach (ListViewItem courseName in listViewCourse.SelectedItems)
            {
                var course = storage.Courses.GetByName(courseName.Text);
                if (course != null)
                {
                    courseCollection.Add(course);
                }
            }

            return courseCollection;
        }

        private void PopulateStudentsCombo()
        {
            var dict = new Dictionary<string, string>();
            foreach (var student in storage.Students.GetAll())
            {
                dict.Add(student.Id, student.FirstName);
            }

            comboBoxStudents.DataSource = new BindingSource(dict, null);
            comboBoxStudents.DisplayMember = "Value";
            comboBoxStudents.ValueMember = "Key";
        }

        private void comboBoxStudents_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var studentId = comboBoxStudents.SelectedValue.ToString();
            Student = storage.Students.Get(studentId);

            DataGridGradesSetUp(Student);
        }

        private void DataGridGradesSetUp(Student student)
        {
            BindingList<Grade> newBindingList = new BindingList<Grade>(student.Grades.ToList());
            var source = new BindingSource(newBindingList, null);

            dataGridViewGrades.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dataGridViewGrades.AutoGenerateColumns = false;
            dataGridViewGrades.AllowUserToAddRows = false;
            dataGridViewGrades.DataSource = source;
            dataGridViewGrades.Columns[0].DataPropertyName = "Name";
            dataGridViewGrades.Columns[1].DataPropertyName = "Score";
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            BindingSource source = (BindingSource)dataGridViewGrades.DataSource;
            BindingList<Grade> grades = (BindingList<Grade>)source.DataSource;

            var student = storage.Students.Get(Student.Id);
            student.Grades = grades.ToList();
        }

        private void InitializeErrorProviders()
        {
            firstNameErrorProvider = new ErrorProvider();
            firstNameErrorProvider.SetIconAlignment(firstNameTextBox, ErrorIconAlignment.MiddleRight);
            firstNameErrorProvider.SetIconPadding(firstNameTextBox, 2);
            firstNameErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            middleNameErrorProvider = new ErrorProvider();
            middleNameErrorProvider.SetIconAlignment(middleNameTextBox, ErrorIconAlignment.MiddleRight);
            middleNameErrorProvider.SetIconPadding(middleNameTextBox, 2);
            middleNameErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            lastNameErrorProvider = new ErrorProvider();
            lastNameErrorProvider.SetIconAlignment(lastNameTextBox, ErrorIconAlignment.MiddleRight);
            lastNameErrorProvider.SetIconPadding(lastNameTextBox, 2);
            lastNameErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            ageErrorProvider = new ErrorProvider();
            ageErrorProvider.SetIconAlignment(ageTextBox, ErrorIconAlignment.MiddleRight);
            ageErrorProvider.SetIconPadding(ageTextBox, 2);
            ageErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            typeErrorProvider = new ErrorProvider();
            typeErrorProvider.SetIconAlignment(groupBoxType, ErrorIconAlignment.MiddleRight);
            typeErrorProvider.SetIconPadding(groupBoxType, 2);
            typeErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            coursesErrorProvider = new ErrorProvider();
            coursesErrorProvider.SetIconAlignment(listViewCourse, ErrorIconAlignment.MiddleRight);
            coursesErrorProvider.SetIconPadding(listViewCourse, 2);
            coursesErrorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
        }

        private void firstNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (firstNameTextBox.Text.Trim() == string.Empty)
            {
                e.Cancel = true;
                firstNameErrorProvider.SetError(firstNameTextBox, "first name is required");
            }
            else
            {
                firstNameErrorProvider.SetError(firstNameTextBox, "");
            }
        }

        private void middleNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (middleNameTextBox.Text.Trim() == string.Empty)
            {
                e.Cancel = true;
                middleNameErrorProvider.SetError(middleNameTextBox, "middle name is required");
            }
            else
            {
                middleNameErrorProvider.SetError(middleNameTextBox, "");
            }
        }

        private void lastNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (lastNameTextBox.Text.Trim() == string.Empty)
            {
                e.Cancel = true;
                lastNameErrorProvider.SetError(lastNameTextBox, "last name is required");
            }
            else
            {
                lastNameErrorProvider.SetError(lastNameTextBox, "");
            }
        }

        private void ageTextBox_Validating(object sender, CancelEventArgs e)
        {

            if (ageTextBox.Text == string.Empty)
            {
                e.Cancel = true;
                ageErrorProvider.SetError(ageTextBox, "age is required");
            }
            else
            {
                int age = 0;
                if (!Int32.TryParse(ageTextBox.Text, out age))
                {
                    e.Cancel = true;
                    ageErrorProvider.SetError(ageTextBox, "age should be number");
                }
                else
                {
                    if (age > 65 || age < 7)
                    {
                        e.Cancel = true;
                        ageErrorProvider.SetError(ageTextBox, "age should be between 7 and 65");
                    }
                    else
                    {
                        ageErrorProvider.SetError(ageTextBox, "");
                    }
                }
            }
        }

        private void groupBoxType_Validating(object sender, CancelEventArgs e)
        {
            if (teacherRadioButton.Checked == false && studentRadioButton.Checked == false)
            {
                e.Cancel = true;
                typeErrorProvider.SetError(groupBoxType, "type is required");
            }
            else
            {
                typeErrorProvider.SetError(groupBoxType, "");
            }
        }

        private void listViewCourse_Validating(object sender, CancelEventArgs e)
        {
            if (listViewCourse.SelectedItems.Count == 0)
            {
                e.Cancel = true;
                coursesErrorProvider.SetError(listViewCourse, "select at least one course");
            }
            else
            {
                coursesErrorProvider.SetError(listViewCourse, "");
            }
        }

        private void comboBoxStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonSave.Enabled = true;
        }
    }
}

﻿using School.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Data
{
    public class Students
    {
        private List<Student> students;

        public Students()
        {
            students = new List<Student>();
        }

        public void Create(Student student)
        {
            foreach (var course in student.Courses)
            {
                student.Grades.Add(new Grade { Score = 0, Course = course });
            }
            students.Add(student);
        }

        public List<Student> GetAll()
        {
            return students.ToList();
        }

        public Student Get(string id)
        {
            return students.Where(s => s.Id == id).FirstOrDefault();
        }
    }
}

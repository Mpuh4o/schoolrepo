﻿using School.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Data
{
    public class Courses
    {
        private List<Course> courses;

        public Courses()
        {
            courses = new List<Course>();
        }

        public void Create(Course course)
        {
            courses.Add(course);
        }

        public List<Course> GetAll()
        {
            return courses.ToList();
        }

        public Course Get(string id)
        {
            return courses.Where(s => s.Id == id).FirstOrDefault();
        }

        public Course GetByName(string name)
        {
            return courses.Where(s => s.Name == name).FirstOrDefault();
        }
    }
}

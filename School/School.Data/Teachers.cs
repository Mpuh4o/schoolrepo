﻿using School.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Data
{
    public class Teachers
    {
        private List<Teacher> teachers;

        public Teachers()
        {
            teachers = new List<Teacher>();
        }

        public void Create(Teacher teacher)
        {
            teachers.Add(teacher);
        }

        public List<Teacher> GetAll()
        {
            return teachers.ToList();
        }

        public Teacher Get(string id)
        {
            return teachers.Where(s => s.Id == id).FirstOrDefault();
        }
    }
}

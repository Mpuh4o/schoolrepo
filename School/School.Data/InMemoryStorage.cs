﻿using School.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Data
{
    /// <summary>
    /// Pre set data
    /// </summary>
    public class InMemoryStorage
    {
        public InMemoryStorage()
        {
            Courses = new Courses();
            Students = new Students();
            Teachers = new Teachers();

            Courses.Create(new Course { Id = Guid.NewGuid().ToString(), Date = new CourseDate { Day = DayOfWeek.Monday, Start = new TimeSpan(9, 30, 0), End = new TimeSpan(10, 30, 0) }, Name = "Math" });
            Courses.Create(new Course { Id = Guid.NewGuid().ToString(), Date = new CourseDate { Day = DayOfWeek.Tuesday, Start = new TimeSpan(11, 00, 0), End = new TimeSpan(12, 30, 0) }, Name = "Biology" });

            Students.Create(new Student { FirstName = "sdvfsdvsd", MiddleName = "sdvdvdfv", LastName = "dvsdvsdvsd", Age = 15, Courses = Courses.GetAll() });
            Students.Create(new Student { FirstName = "sdvfsdvsd", MiddleName = "sdvdvdfv", LastName = "dvsdvsdvsd", Age = 15, Courses = Courses.GetAll() });
            Students.Create(new Student { FirstName = "sdvfsdvsd", MiddleName = "sdvdvdfv", LastName = "dvsdvsdvsd", Age = 15, Courses = Courses.GetAll() });

            Teachers.Create(new Teacher { FirstName = "sdvsdvsdvs", MiddleName = "sdvsdvsd", LastName = "sdvsdvsd", Age = 34, Courses = Courses.GetAll() });
            Teachers.Create(new Teacher { FirstName = "sdvsdvsdvs", MiddleName = "sdvsdvsd", LastName = "sdvsdvsd", Age = 34, Courses = Courses.GetAll() });
            Teachers.Create(new Teacher { FirstName = "sdvsdvsdvs", MiddleName = "sdvsdvsd", LastName = "sdvsdvsd", Age = 34, Courses = Courses.GetAll() });
        }

        public Students Students { get; set; }
        public Courses Courses { get; set; }
        public Teachers Teachers { get; set; }
    }
}
